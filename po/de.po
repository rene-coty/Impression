# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the impression package.
# Philipp Kiemle <philipp.kiemle@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: impression\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-20 21:04-0400\n"
"PO-Revision-Date: 2023-11-18 00:29+0100\n"
"Last-Translator: Philipp Kiemle <philipp.kiemle@gmail.com>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.4.1\n"

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:3
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:5
#: data/resources/blueprints/window.blp:9
#: data/resources/blueprints/window.blp:21
#: data/resources/blueprints/window.blp:146 src/main.rs:57 src/window.rs:424
msgid "Impression"
msgstr "Impression"

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:4
msgid "Media Writer"
msgstr "Medien-Schreibprogramm"

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:5
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:11
msgid "Create bootable drives"
msgstr "Bootfähige Laufwerke erstellen"

#: data/io.gitlab.adhami3310.Impression.desktop.in.in:11
msgid "usb;flash;writer;bootable;drive;iso;img;disk;image"
msgstr ""
"usb;flash;writer;Schreiber;Brenner;bootable;bootfähig;drive;Laufwerk;iso;img;"
"disk;Festplatte;Image;Abbild"

#: data/io.gitlab.adhami3310.Impression.gschema.xml.in:6
msgid "Window width"
msgstr "Fensterberite"

#: data/io.gitlab.adhami3310.Impression.gschema.xml.in:10
msgid "Window height"
msgstr "Fensterhöhe"

#: data/io.gitlab.adhami3310.Impression.gschema.xml.in:14
msgid "Window maximized state"
msgstr "Maximierter Zustand des Fensters"

#. developer_name tag deprecated with Appstream 1.0
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:7
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:9
msgid "Khaleel Al-Adhami"
msgstr "Khaleel Al-Adhami"

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:17
msgid ""
"Write disk images to your drives with ease. Select an image, insert your "
"drive, and you're good to go! Impression is a useful tool for both avid "
"distro-hoppers and casual computer users."
msgstr ""
"Schreiben Sie Datenträgerabbilder mit Leichtigkeit. Wählen Sie ein Abbild "
"aus, verbinden Sie den Datenträger und Sie sind startklar! Impression ist "
"ein nützliches Werkzeug - sowohl für Distro-Hopper als auch für "
"gelegentliche Computerbenutzer."

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:38
msgid "Screen with a choice of a local image or internet download"
msgstr ""
"Bildschirm mit einer Auswahlmöglichkeit für eine lokale Abbilddatei oder "
"einen Download aus dem Internet"

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:42
msgid "Screen with a chosen ISO and available USB memories"
msgstr ""
"Bildschirm mit einer ausgewählten ISO-Datei und verfügbaren USB-Speichern"

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:46
msgid "Writing the ISO in progress"
msgstr "ISO-Datei wird geschrieben"

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:50
msgid "Success screen with a big check mark"
msgstr "Erfolgsbildschirm mit einem großen Haken"

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:70
msgid "This is a minor release of Impression with a few small improvements."
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:72
msgid "Support for .xz files"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:73
#, fuzzy
msgid "Faster downloading of images"
msgstr "Schreiben des Datenträgerabbilds fehlgeschlagen"

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:75
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:91
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:102
#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:117
msgid ""
"Impression is made possible by volunteer developers, designers, and "
"translators. Thank you for your contributions!"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:80
msgid ""
"This minor release fixes a bug where some .ISO files wouldn't be detected."
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:85
msgid ""
"This release of Impression brings even more convenience and power to your "
"fingertips:"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:87
msgid "Direct download of a selection of images directly from the app"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:88
msgid "Updated and more understandable terminology"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:89
msgid "Security fixes"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:96
msgid "This minor release of Impression delivers:"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:98
msgid "Support for mobile screen sizes"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:99
msgid "Various bug fixes, improving reliability and stability"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:100
msgid ""
"Brazillian Portugese translations, making Impression available in a total of "
"9 languages"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:107
msgid ""
"This major release of Impression brings a bunch of exciting improvements:"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:109
msgid ""
"Visual enhancements to make the app more beautiful, focused, and engaging"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:110
msgid "Automatic updates of the available drives list"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:111
msgid "Explicit drive selection before flashing, to avoid accidental data loss"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:112
msgid ""
"Turkish and Czech translations, making Impression available in a total of 8 "
"languages"
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:114
msgid ""
"The versioning scheme has been simplified to only include major and minor "
"versions. The previous version of Impression was 1.0.1, this is version 2.0. "
"Going forward, new features and noticeable changes will be included in new "
"major releases, while fixes and translations will result in new minor "
"releases."
msgstr ""

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:122
#, fuzzy
msgid "Added Spanish, French, German, Russian, and Italian translations."
msgstr ""
"Übersetzungen für Spanisch, Französisch, Deutsch, Russisch und Italienisch "
"hinzugefügt."

#: data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in:127
msgid "Initial version."
msgstr "Anfängliche Version."

#: data/resources/blueprints/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Allgemein"

#: data/resources/blueprints/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Open File"
msgstr "Datei öffnen"

#: data/resources/blueprints/help-overlay.blp:19
msgctxt "shortcut window"
msgid "New Window"
msgstr "Neues Fenster"

#: data/resources/blueprints/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Tastenkürzel anzeigen"

#: data/resources/blueprints/help-overlay.blp:29
msgctxt "shortcut window"
msgid "Quit"
msgstr "Beenden"

#: data/resources/blueprints/window.blp:25
msgid "Drop Here to Open"
msgstr ""

#: data/resources/blueprints/window.blp:35
#: data/resources/blueprints/window.blp:156
#: data/resources/blueprints/window.blp:254
msgid "Main Menu"
msgstr "Hauptmenü"

#: data/resources/blueprints/window.blp:54
msgid "Choose Image"
msgstr "Abbild auswählen"

#: data/resources/blueprints/window.blp:64
msgid "Open File…"
msgstr "Datei öffnen …"

#: data/resources/blueprints/window.blp:74
msgid "Direct Download"
msgstr "Direkter Download"

#: data/resources/blueprints/window.blp:108
msgid "No Connection"
msgstr "Keine Verbindung"

#: data/resources/blueprints/window.blp:109
msgid "Connect to the internet to view available images"
msgstr ""
"Stellen Sie eine Internetverbindung her, um die verfügbaren Abbilddateien zu "
"sehen"

#: data/resources/blueprints/window.blp:218
msgid "All data on the selected drive will be erased"
msgstr "Alle Daten auf dem ausgewählten Laufwerk werden gelöscht"

#: data/resources/blueprints/window.blp:221
msgid "Write"
msgstr "Schreiben"

#: data/resources/blueprints/window.blp:283
msgid "No Drives"
msgstr "Keine Laufwerke"

#: data/resources/blueprints/window.blp:284
msgid "Insert a drive to write to"
msgstr "Legen Sie ein Laufwerk ein, auf das geschrieben werden soll"

#: data/resources/blueprints/window.blp:295
msgid "Writing Completed"
msgstr "Schreiben abgeschlossen"

#: data/resources/blueprints/window.blp:296
msgid "The drive can be safely removed"
msgstr "Das Laufwerk kann sicher entfernt werden"

#: data/resources/blueprints/window.blp:305
msgid "Finish"
msgstr "Ende"

#: data/resources/blueprints/window.blp:326
msgid "Writing Unsuccessful"
msgstr "Schreiben fehlgeschlagen"

#: data/resources/blueprints/window.blp:335
msgid "Retry"
msgstr "Erneut versuchen"

#: data/resources/blueprints/window.blp:355 src/window.rs:331 src/window.rs:365
msgid "Writing…"
msgstr "Schreiben …"

#: data/resources/blueprints/window.blp:357 src/window.rs:330
msgid "Do not remove the drive"
msgstr "Das Laufwerk darf nicht entfernt werden"

#: data/resources/blueprints/window.blp:372 src/window.rs:250 src/window.rs:286
msgid "_Cancel"
msgstr "_Abbrechen"

#: data/resources/blueprints/window.blp:400
msgid "Keyboard Shortcuts"
msgstr "Tastenkürzel"

#: data/resources/blueprints/window.blp:405
msgid "About Impression"
msgstr "Info zu Impression"

#: src/window.rs:246
msgid "Stop Writing?"
msgstr "Schreiben beenden?"

#: src/window.rs:247
msgid "This might leave the drive in a faulty state."
msgstr ""
"Dies kann dazu führen, dass sich das Laufwerk in einem fehlerhaften Zustand "
"befindet."

#: src/window.rs:250
msgid "_Stop"
msgstr "_Stopp"

#: src/window.rs:279
msgid "Erase Drive?"
msgstr "Laufwerk löschen?"

#: src/window.rs:287
msgid "_Erase"
msgstr "_Löschen"

#: src/window.rs:324 src/window.rs:353
msgid "Writing will begin once the download is completed"
msgstr "Das Schreiben beginnt, sobald der Download abgeschlossen ist"

#: src/window.rs:326 src/window.rs:362
msgid "Downloading Image…"
msgstr "Abbild wird heruntergeladen …"

#: src/window.rs:356
msgid "Copying files…"
msgstr "Dateien werden kopiert …"

#: src/window.rs:391
msgid "Failed to write image"
msgstr "Schreiben des Datenträgerabbilds fehlgeschlagen"

#: src/window.rs:399
msgid "Image Written"
msgstr "Datenträgerabbild wurde geschrieben"

#: src/window.rs:603
msgid "Disk Images"
msgstr "Datenträgerabbilder"

#: src/window.rs:629
msgid "File is not a Disk Image"
msgstr "Datei ist kein Datenträgerabbild"

#: src/window.rs:734
msgid "translator-credits"
msgstr ""
"FineFindus <https://github.com/FineFindus>\n"
"Philipp Kiemle <philipp.kiemle@gmail.com>"

#: src/window.rs:736
msgid "Code borrowed from"
msgstr "Code angelehnt an"

#: src/flash.rs:95
#, fuzzy
msgid "Failed to open disk"
msgstr "Schreiben des Datenträgerabbilds fehlgeschlagen"

#: src/flash.rs:145
#, fuzzy
msgid "Failed to extract drive"
msgstr "Schreiben des Datenträgerabbilds fehlgeschlagen"

#: src/flash.rs:203
#, fuzzy
msgid "Failed to download image"
msgstr "Schreiben des Datenträgerabbilds fehlgeschlagen"

#: src/flash.rs:256
#, fuzzy
msgid "Writing to disk failed"
msgstr "Schreiben abgeschlossen"

#~ msgid "usb"
#~ msgstr "USB"

#~ msgid "writer"
#~ msgstr "Schreiber"

#~ msgid "flash"
#~ msgstr "brennen"

#~ msgid "bootable"
#~ msgstr "bootfähig"

#~ msgid "drive"
#~ msgstr "Laufwerk"

#~ msgid "iso"
#~ msgstr "iso"

#~ msgid "img"
#~ msgstr "img"

#~ msgid "disk"
#~ msgstr "Festplatte"

#~ msgid "image"
#~ msgstr "Datenträgerabbild"

#~ msgid "Validating…"
#~ msgstr "Überprüfen …"

#~ msgid "Flash"
#~ msgstr "Brennen"

#~ msgid "Flashing…"
#~ msgstr "Brennen…"

#~ msgid "New Window"
#~ msgstr "Neues Fenster"

#~ msgid "Image flashed"
#~ msgstr "Datenträgerabbild gebrannt"

#~ msgid "flash;usb;drive;bootable"
#~ msgstr "flash;usb;laufwerk;bootfähig"

#~ msgid "Refresh Devices"
#~ msgstr "Geräte aktualisieren"

#~ msgid "Image Flashed"
#~ msgstr "Datenträgerabbild gebrannt"

#~ msgid "Flash disk images (ISOs/IMGs) to create bootable devices"
#~ msgstr ""
#~ "Brenne Datenträgerabbilder (ISOs/IMGs) zur Erstellung bootfähiger Geräte"

#~ msgid "Flash Images"
#~ msgstr "Datenträgerabbild Brennen"

#, fuzzy
#~ msgid "Open a disk image to flash to a USB drive"
#~ msgstr ""
#~ "Öffne ein Datenträgerabbild, um es auf ein USB-Laufwerk zu übertragen."

#, fuzzy
#~ msgid "Warning: This will erase all data on the selected drive"
#~ msgstr ""
#~ "Achtung: Dies wird alle Daten auf dem ausgewählten Laufwerk überschreiben."

#~ msgid "Updating device list…"
#~ msgstr "Gerätliste wird aktualisiert…"

#~ msgid "Have a nice day!"
#~ msgstr "Einen schönen Tag noch!"

#~ msgid "Done"
#~ msgstr "Fertig"

#~ msgid "Flash Again"
#~ msgstr "Erneut Brennen"

#~ msgid "Open File"
#~ msgstr "Datei Öffnen"

#~ msgid "Primary Menu"
#~ msgstr "Primäres Menü"
